# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-tools
pkgver=1.2.169
pkgrel=0
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Utilities and Tools"
license="Apache-2.0"
depends="vulkan-loader"
makedepends="
	cmake
	glslang-dev
	libx11-dev
	libxrandr-dev
	python3
	vulkan-headers
	vulkan-loader-dev
	wayland-dev
	"
source="https://github.com/KhronosGroup/Vulkan-Tools/archive/v$pkgver/vulkan-tools-v$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/Vulkan-Tools-$pkgver"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_SKIP_RPATH=True \
		-DBUILD_CUBE=ON \
		-DBUILD_VULKANINFO=ON \
		-DGLSLANG_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="00369e21c2055983eb3bd85767d224aa9ac2d3baf6e6394af1ee391820135692aa6b4189a2c068a84330bc1c170b41495d5c00bde4aa5d6b3fab0db64121df4b  vulkan-tools-v1.2.169.tar.gz"
